import React from 'react'
import './Footer.css'

function Footer() {
    return (
        <>
            <footer className="site-footer">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12 col-md-6">
                            <img src="https://media.licdn.com/dms/image/C4D0BAQF7hT8ht6VyRQ/company-logo_200_200/0/1639764272786?e=1681344000&v=beta&t=-bjzbtHCws9lTkeAcZCqNH73lZUnYnhJw27YEc4O-xE" alt="" width={100} />

                            <ul className="footer-links">
                                <li><h6 className='text-justify'>Marketplug Company Ltd</h6></li>
                                <li><a className="text-justify">Westminster Abbey</a></li>
                                <li><a className="text-justify">21-05B Newton's Plaza,</a></li>
                                <li><a className="text-justify">London SW1A 2HQ</a></li>
                            </ul>
                        </div>

                        <div className="col-xs-6 col-md-2">
                            <h6>Contacts</h6>
                            <ul className="footer-links">
                                <li><a href=""><u>info@marketplug.com</u></a></li>
                                <li><a href=""><u>LinkedIn</u></a></li>
                            </ul>
                        </div>

                        <div className="col-xs-6 col-md-2">
                            <h6>True & Legal</h6>
                            <ul className="footer-links">
                                <li><a href=""><u>Terms & Conditions</u></a></li>
                                <li><a href=""><u>Privacy Policy</u></a></li>
                            </ul>
                        </div>

                        <div className="col-xs-6 col-md-2">
                            <h6><u>Whitepaper</u></h6>
                            <h6><u>FAQs</u></h6>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <h6><b>Stay up to date</b></h6>
                            <form action="#">
                                <input type="email" className='email-text' placeholder="Enter your email address" />
                                <button target="_blank" className=" mx-4 submit-btn" style={{ textDecoration: 'none' }} to='/whitepaper'>Join Community</button>
                            </form>
                        </div>
                    </div>
                    <hr />
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center">
                            <p className="copyright-text">©2023 Marketplug. All rights reserved</p>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    )
}

export default Footer
