import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from "./components/Header";
import Home from "./components/Home";
import Platform from "./components/Platform";
import About from "./components/About";
import Roadmap from "./components/Roadmap";
import WhitePaper from "./components/WhitePaper";
import Footer from "./components/Footer";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route exact path="/" element={<Home />}></Route>
          <Route exact path="/platform" element={<Platform />}></Route>
          <Route exact path="/about" element={<About />}></Route>
          <Route exact path="/roadmap" element={<Roadmap />}></Route>
          <Route exact path="/whitepaper" element={<WhitePaper />}></Route>
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
